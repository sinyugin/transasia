package com.example.pokeapp.model;

import javax.inject.Inject;

public class Card {

    private String   name,
                    sprite,
                    speed,
                    special_defence,
                    special_attack,
                    ability1,
                    ability2,
                    ability3;

    private int number;

    @Inject
    public Card() {
    }

    public static Card getInstance(){
        return new Card();
    }

    public int getNumber() {
        return number;
    }

    public void setNumber(int number) {
        this.number = number;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSprite() {
        return sprite;
    }

    public void setSprite(String sprite) {
        this.sprite = sprite;
    }

    public String getSpeed() {
        return speed;
    }

    public void setSpeed(String speed) {
        this.speed = speed;
    }

    public String getSpecial_defence() {
        return special_defence;
    }

    public void setSpecial_defence(String special_defence) {
        this.special_defence = special_defence;
    }

    public String getSpecial_attack() {
        return special_attack;
    }

    public void setSpecial_attack(String special_attack) {
        this.special_attack = special_attack;
    }

    public String getAbility1() {
        return ability1;
    }

    public void setAbility1(String ability1) {
        this.ability1 = ability1;
    }

    public String getAbility2() {
        return ability2;
    }

    public void setAbility2(String ability2) {
        this.ability2 = ability2;
    }

    public String getAbility3() {
        return ability3;
    }

    public void setAbility3(String ability3) {
        this.ability3 = ability3;
    }
}
