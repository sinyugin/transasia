package com.example.pokeapp.model;

import com.google.gson.annotations.SerializedName;


public class Pokemon {

    @SerializedName("url")
    private String url;

    @SerializedName("name")
    private String name;
    private int number;
    private String sprite;
    private int order;

    public Pokemon() {
    }

    public static Pokemon getInstance(){
        return new Pokemon();
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getNumber() {
        String[] part = url.split("/");
        return Integer.parseInt(part[part.length -1]);
    }

    public void setSprite(String sprite) {
        this.sprite = sprite;
    }

    public int getOrder() {
        return order;
    }

    public void setOrder(int order) {
        this.order = order;
    }

}
