package com.example.pokeapp.db;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import com.example.pokeapp.app.MyConstants;

import static com.example.pokeapp.app.MyConstants.DB_NAME;
import static com.example.pokeapp.app.MyConstants.TABLE_VERSION;


public class MySQLiteHelper extends SQLiteOpenHelper {

    public MySQLiteHelper(Context context) {
        super(context, DB_NAME, null, TABLE_VERSION );
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL(MyConstants.createPokemonTable());
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        //no need
    }
}
