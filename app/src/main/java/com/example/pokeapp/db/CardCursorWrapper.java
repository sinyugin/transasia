package com.example.pokeapp.db;

import android.database.Cursor;
import android.database.CursorWrapper;

import com.example.pokeapp.model.Card;
import com.example.pokeapp.model.Pokemon;

import static com.example.pokeapp.app.MyConstants.ABILITY1;
import static com.example.pokeapp.app.MyConstants.ABILITY2;
import static com.example.pokeapp.app.MyConstants.ABILITY3;
import static com.example.pokeapp.app.MyConstants.NAME;
import static com.example.pokeapp.app.MyConstants.NUMBER;
import static com.example.pokeapp.app.MyConstants.SPECIAL_ATTACK;
import static com.example.pokeapp.app.MyConstants.SPECIAL_DEFENCE;
import static com.example.pokeapp.app.MyConstants.SPEED;
import static com.example.pokeapp.app.MyConstants.SPRITE;


public class CardCursorWrapper extends CursorWrapper {

    private CardCursorWrapper(Cursor cursor) {
        super(cursor);
    }

    public static CardCursorWrapper getInstance(Cursor cursor){
        return new CardCursorWrapper(cursor);
    }

    public Card getCard(){
        Card card = Card.getInstance();

        int number = getInt(getColumnIndex(NUMBER));
        String name = getString(getColumnIndex(NAME));
        String sprite = getString(getColumnIndex(SPRITE));
        String speed = getString(getColumnIndex(SPEED));
        String special_defence = getString(getColumnIndex(SPECIAL_DEFENCE));
        String special_attack = getString(getColumnIndex(SPECIAL_ATTACK));
        String ability1 = getString(getColumnIndex(ABILITY1));
        String ability2 = getString(getColumnIndex(ABILITY2));
        String ability3 = getString(getColumnIndex(ABILITY3));

        card.setNumber(number);
        card.setName(name);
        card.setSprite(sprite);
        card.setSpeed(speed);
        card.setSpecial_defence(special_defence);
        card.setSpecial_attack(special_attack);
        if (ability1 != null) card.setAbility1(ability1);
        if (ability2 != null) card.setAbility2(ability2);
        if (ability3 != null) card.setAbility3(ability3);

        return card;
    }

    public Pokemon getPokemon(){
        Pokemon pokemon = Pokemon.getInstance();

        String name = getString(getColumnIndex(NAME));
        String sprite = getString(getColumnIndex(SPRITE));
        int order = Integer.parseInt(getString(getColumnIndex(NUMBER)));

        pokemon.setName(name);
        pokemon.setSprite(sprite);
        pokemon.setOrder(order);

        return pokemon;
    }
}
