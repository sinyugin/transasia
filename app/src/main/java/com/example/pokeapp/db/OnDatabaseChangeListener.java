package com.example.pokeapp.db;



public interface OnDatabaseChangeListener {

    void setSavedState(boolean isSaved);
}
