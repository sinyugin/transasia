package com.example.pokeapp.db;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import com.example.pokeapp.model.Card;
import com.example.pokeapp.model.Pokemon;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

import dagger.Module;
import dagger.Provides;

import static com.example.pokeapp.app.MyConstants.ABILITY1;
import static com.example.pokeapp.app.MyConstants.ABILITY2;
import static com.example.pokeapp.app.MyConstants.ABILITY3;
import static com.example.pokeapp.app.MyConstants.NAME;
import static com.example.pokeapp.app.MyConstants.NUMBER;
import static com.example.pokeapp.app.MyConstants.POKEMON_TABLE;
import static com.example.pokeapp.app.MyConstants.SPECIAL_ATTACK;
import static com.example.pokeapp.app.MyConstants.SPECIAL_DEFENCE;
import static com.example.pokeapp.app.MyConstants.SPEED;
import static com.example.pokeapp.app.MyConstants.SPRITE;


public class DatabaseManager {

    private SQLiteDatabase mWriteDB, mReadDB;
    private MySQLiteHelper mDatabaseHelper;
    private static DatabaseManager sInstance;
    private OnDatabaseChangeListener mListener;


    public DatabaseManager(Context context){
        mDatabaseHelper = new MySQLiteHelper(context);
    }

    public static DatabaseManager getInstance(Context context){
        if (sInstance == null){
            sInstance = new DatabaseManager(context);
        }
        return sInstance;
    }

    public void setListener(OnDatabaseChangeListener listener) {
        mListener = listener;
    }

    private static ContentValues getContentValues(Card card){
        ContentValues values = new ContentValues();
        values.put(NUMBER, card.getNumber());
        values.put(NAME, card.getName());
        values.put(SPRITE, card.getSprite());
        values.put(SPEED, card.getSpeed());
        values.put(SPECIAL_DEFENCE, card.getSpecial_defence());
        values.put(SPECIAL_ATTACK, card.getSpecial_attack());
        values.put(ABILITY1, card.getAbility1());
        values.put(ABILITY2, card.getAbility2());
        values.put(ABILITY3, card.getAbility3());
        return values;
    }

    private void closeWriteDB() {
        mWriteDB.close();
    }

    private void getWriteDB() {
        mWriteDB = mDatabaseHelper.getWritableDatabase();
    }

    public void insertCard(Card card) {
        getWriteDB();
        ContentValues values = getContentValues(card);
        mWriteDB.insert(POKEMON_TABLE, null, values);
        if (mListener != null) mListener.setSavedState(true);
        closeWriteDB();
    }

    public void deleteCard(int number) {
        getWriteDB();
        mWriteDB.delete(POKEMON_TABLE, NUMBER + " = ?", new String[] {String.valueOf(number)});
        if (mListener != null) mListener.setSavedState(false);
        closeWriteDB();
    }

    public Card getCard(int number){
        CardCursorWrapper cursor = CardCursorWrapper.getInstance(queryCard(NUMBER + " = ?", new String[]{String.valueOf(number)}));
        try {
            if (cursor.getCount() == 0) return null;
            cursor.moveToFirst();
            return cursor.getCard();
        } finally {
            cursor.close();
        }
    }
    private CardCursorWrapper queryCard(String whereClause, String[] whereArgs){
        mReadDB = mDatabaseHelper.getReadableDatabase();
        Cursor cursor = mReadDB.query(
                POKEMON_TABLE,
                null,
                whereClause,
                whereArgs,
                null,
                null,
                null
        );
        return CardCursorWrapper.getInstance(cursor);
    }

    public List<Pokemon> getPokemons(){
        List<Pokemon> pokemonList = new ArrayList<>();
        CardCursorWrapper cursor = CardCursorWrapper.getInstance(queryPokemons());

        try {
            if (cursor.getCount() == 0) return null;
            cursor.moveToFirst();
            while (!cursor.isAfterLast()){
                Pokemon pokemon = cursor.getPokemon();
                pokemonList.add(pokemon);
                cursor.moveToNext();
            }
            return pokemonList;
        } finally {
            cursor.close();
        }
    }

    private CardCursorWrapper queryPokemons(){
        String[] columns = {NAME, SPRITE, NUMBER};
        mReadDB = mDatabaseHelper.getReadableDatabase();
        Cursor cursor = mReadDB.query(
                POKEMON_TABLE,
                columns,
                null,
                null,
                null,
                null,
                null
        );
        return CardCursorWrapper.getInstance(cursor);
    }



}
