package com.example.pokeapp;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.example.pokeapp.db.DatabaseManager;
import com.example.pokeapp.db.OnDatabaseChangeListener;
import com.example.pokeapp.model.Card;
import com.facebook.drawee.view.SimpleDraweeView;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import dagger.android.AndroidInjection;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.observers.DisposableSingleObserver;
import io.reactivex.schedulers.Schedulers;

import static com.example.pokeapp.app.MyApp.sApiService;
import static com.example.pokeapp.app.MyConstants.ABILITY1;
import static com.example.pokeapp.app.MyConstants.ABILITY2;
import static com.example.pokeapp.app.MyConstants.ABILITY3;
import static com.example.pokeapp.app.MyConstants.ARG_DETAIL_ACTIVITY_NUMBER;
import static com.example.pokeapp.app.MyConstants.ARG_DETAIL_ACTIVITY_STATE;
import static com.example.pokeapp.app.MyConstants.NAME;
import static com.example.pokeapp.app.MyConstants.SPECIAL_ATTACK;
import static com.example.pokeapp.app.MyConstants.SPECIAL_DEFENCE;
import static com.example.pokeapp.app.MyConstants.SPEED;
import static com.example.pokeapp.app.MyConstants.SPRITE;


public class DetailStreamActivity extends AppCompatActivity implements OnDatabaseChangeListener{

    @BindView(R.id.progressBar)ProgressBar mProgressBar;
    @BindView(R.id.sprite) SimpleDraweeView sprite;
    @BindView(R.id.name) TextView name;
    @BindView(R.id.speed) TextView speed;
    @BindView(R.id.attack) TextView attack;
    @BindView(R.id.defence) TextView defence;
    @BindView(R.id.ability1) TextView ability1;
    @BindView(R.id.ability2) TextView ability2;
    @BindView(R.id.ability3) TextView ability3;
    @BindView(R.id.FAB) FloatingActionButton mFAB;

    @Inject Card mCard;

    private int mPokemonNumber;
    private DatabaseManager mManager = DatabaseManager.getInstance(this);
    private boolean mIsSavedState;
    private CompositeDisposable mCompositeDisposable = new CompositeDisposable();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detail);
        AndroidInjection.inject(this);
        ButterKnife.bind(this);
        mManager.setListener(this);
        mPokemonNumber = getIntent().getIntExtra(ARG_DETAIL_ACTIVITY_NUMBER, 1);

        if (savedInstanceState == null){
            try {
                mCard = mManager.getCard(mPokemonNumber);
                setViews(mCard);
                mIsSavedState = true;
                mFAB.setImageDrawable(getResources().getDrawable(R.drawable.ic_delete_24dp));
            } catch (Exception e){
                mCard = fetchPokemonAtObservableStream(mPokemonNumber);
                mCard.setSprite(prepareUriToSet(mPokemonNumber));
                mCard.setNumber(mPokemonNumber);
                mIsSavedState = false;
                mFAB.setImageDrawable(getResources().getDrawable(R.drawable.ic_save_24dp));
            }
        } else {
            mIsSavedState = savedInstanceState.getBoolean(ARG_DETAIL_ACTIVITY_STATE);
            if (mIsSavedState){
                mFAB.setImageDrawable(getResources().getDrawable(R.drawable.ic_delete_24dp));
            } else {
                mFAB.setImageDrawable(getResources().getDrawable(R.drawable.ic_save_24dp));
            }
        }

        mFAB.setOnClickListener(v -> {
            if (mIsSavedState){
                mManager.deleteCard(mCard.getNumber());
            } else {
                mManager.insertCard(mCard);
            }

        });
    }

    @Override
    public void setSavedState(boolean isSaved) {
        mIsSavedState = isSaved;
        if (mIsSavedState){
            mFAB.setImageDrawable(getResources().getDrawable(R.drawable.ic_delete_24dp));
        } else {
            mFAB.setImageDrawable(getResources().getDrawable(R.drawable.ic_save_24dp));
        }
    }


    public void setViews(Card card){
        name.setText(card.getName());
        sprite.setImageURI(Uri.parse(card.getSprite()));
        speed.setText(card.getSpeed());
        defence.setText(card.getSpecial_defence());
        attack.setText(card.getSpecial_attack());

        try {
            ability1.setText(card.getAbility1());
            ability1.setVisibility(View.VISIBLE);
        } catch (Exception e){
            Log.e("error", e.getLocalizedMessage());
        }

        try {
            ability2.setText(card.getAbility2());
            ability2.setVisibility(View.VISIBLE);
        } catch (IndexOutOfBoundsException e){
            Log.e("error", e.getLocalizedMessage());
        }

        try {
            ability3.setText(card.getAbility3());
            ability3.setVisibility(View.VISIBLE);
        } catch (IndexOutOfBoundsException e){
            Log.e("error", e.getLocalizedMessage());
        }
    }

    @NonNull
    private String prepareUriToSet(int mPokemonNumber) {
        StringBuilder builder = new StringBuilder();
        builder.append("https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/");
        builder.append(mPokemonNumber);
        builder.append(".png");
        return builder.toString();
    }

    public static void showActivity(Context context, int pokemonNumber){
        Intent intent = new Intent(context, DetailStreamActivity.class);
        intent.putExtra(ARG_DETAIL_ACTIVITY_NUMBER, pokemonNumber);
        context.startActivity(intent);
    }


    private Card fetchPokemonAtObservableStream(int order){
       Card card = new Card();
        mFAB.setVisibility(View.INVISIBLE);
        mProgressBar.setVisibility(View.VISIBLE);

        mCompositeDisposable.add(
                sApiService.fetchPokemonAtObservableStream(order)
                        .subscribeOn(Schedulers.io())
                        .observeOn(AndroidSchedulers.mainThread())
                        .subscribeWith(new DisposableSingleObserver<com.example.pokeapp.model.entirePokemon.Response>(){
                            @Override
                            public void onSuccess(com.example.pokeapp.model.entirePokemon.Response result){
                                String pok_name = result.getName().toUpperCase();
                                card.setName(pok_name);
                                String pok_speed = String.valueOf(result.getStats().get(0).getBaseStat());
                                card.setSpeed(pok_speed);
                                String pok_defence = String.valueOf(result.getStats().get(1).getBaseStat());
                                card.setSpecial_defence(pok_defence);
                                String pok_attack = String.valueOf(result.getStats().get(2).getBaseStat());
                                card.setSpecial_attack(pok_attack);
                                try {
                                    String pok_ability1 = result.getAbilities().get(0).getAbility().getName();
                                    card.setAbility1(pok_ability1);
                                } catch (IndexOutOfBoundsException e){
                                    Log.e("error", "No value for ability 1");
                                }

                                try {
                                    String pok_ability2 = result.getAbilities().get(1).getAbility().getName();
                                    card.setAbility2(pok_ability2);
                                } catch (IndexOutOfBoundsException e){
                                    Log.e("error", "No value for ability 1");
                                }

                                try {
                                    String pok_ability3 = result.getAbilities().get(2).getAbility().getName();
                                    card.setAbility3(pok_ability3);
                                } catch (IndexOutOfBoundsException e){
                                    Log.e("error", "No value for ability 1");
                                }

                                setViews(card);
                                mFAB.setVisibility(View.VISIBLE);
                                mProgressBar.setVisibility(View.INVISIBLE);
                                mProgressBar.setVisibility(View.INVISIBLE);
                            }
                            @Override
                            public void onError(Throwable e) {
                                Log.e("tag", "onError: " + e.getMessage());
                            }
                        })
        );
        return card;
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putBoolean(ARG_DETAIL_ACTIVITY_STATE, mIsSavedState);
        outState.putInt(ARG_DETAIL_ACTIVITY_NUMBER, mPokemonNumber);
        outState.putString(NAME, mCard.getName());
        outState.putString(SPRITE, mCard.getSprite());
        outState.putString(SPEED, mCard.getSpeed());
        outState.putString(SPECIAL_DEFENCE, mCard.getSpecial_defence());
        outState.putString(SPECIAL_ATTACK, mCard.getSpecial_attack());
        outState.putString(ABILITY1, mCard.getAbility1());
        outState.putString(ABILITY2, mCard.getAbility2());
        outState.putString(ABILITY3, mCard.getAbility3());
    }

    @Override
    protected void onRestoreInstanceState(Bundle savedInstanceState) {
        super.onRestoreInstanceState(savedInstanceState);
        mPokemonNumber = savedInstanceState.getInt(ARG_DETAIL_ACTIVITY_NUMBER, 1);
        mCard.setNumber(mPokemonNumber);
        mCard.setName(savedInstanceState.getString(NAME));
        mCard.setSprite(savedInstanceState.getString(SPRITE));
        mCard.setSpeed(savedInstanceState.getString(SPEED));
        mCard.setSpecial_defence(savedInstanceState.getString(SPECIAL_DEFENCE));
        mCard.setSpecial_attack(savedInstanceState.getString(SPECIAL_ATTACK));
        mCard.setAbility1(savedInstanceState.getString(ABILITY1));
        mCard.setAbility2(savedInstanceState.getString(ABILITY2));
        mCard.setAbility3(savedInstanceState.getString(ABILITY3));
        setViews(mCard);
    }
}
