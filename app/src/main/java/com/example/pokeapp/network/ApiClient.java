package com.example.pokeapp.network;

import java.util.concurrent.TimeUnit;

import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Retrofit;
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory;
import retrofit2.converter.gson.GsonConverterFactory;

import static com.example.pokeapp.app.MyConstants.BASE_URL;
import static com.example.pokeapp.app.MyConstants.REQUEST_TIMEOUT;

public class ApiClient {

    private static Retrofit sRetrofit = null;
    private static OkHttpClient sOkHttpClient = null;

    public static Retrofit getClient(){
        if (sOkHttpClient == null)
            initOkHttp();

        if (sRetrofit == null){
            sRetrofit = new Retrofit.Builder()
                    .baseUrl(BASE_URL)
                    .client(sOkHttpClient)
                    .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                    .addConverterFactory(GsonConverterFactory.create())
                    .build();
        }
        return sRetrofit;
    }

    private static void initOkHttp() {
        OkHttpClient.Builder client = new OkHttpClient.Builder()
                .connectTimeout(REQUEST_TIMEOUT, TimeUnit.SECONDS)
                .readTimeout(REQUEST_TIMEOUT, TimeUnit.SECONDS);

        HttpLoggingInterceptor interceptor = new HttpLoggingInterceptor();
        interceptor.setLevel(HttpLoggingInterceptor.Level.BODY);

        client.addInterceptor(interceptor);
        sOkHttpClient = client.build();
    }
}
