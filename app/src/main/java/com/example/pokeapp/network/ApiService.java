package com.example.pokeapp.network;

import com.example.pokeapp.model.PokemonList;
import com.example.pokeapp.model.entirePokemon.Response;

import io.reactivex.Single;
import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Path;
import retrofit2.http.Query;


public interface ApiService {
    @GET("pokemon/{order}/")
    Call<Response> fetchPokemon(@Path("order") int order);

    @GET("pokemon/")
    Single<PokemonList> fetchPokemonsWithPagination(@Query("limit") int limit, @Query("offset") int offset);

    @GET("pokemon/{order}/")
    Single<Response> fetchPokemonAtObservableStream(@Path("order") int order);
}
