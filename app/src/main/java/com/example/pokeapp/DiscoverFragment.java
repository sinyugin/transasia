package com.example.pokeapp;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.RecyclerView.OnScrollListener;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;

import com.example.pokeapp.adapter.PokemonAdapter;
import com.example.pokeapp.model.Pokemon;
import com.example.pokeapp.model.PokemonList;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.observers.DisposableSingleObserver;
import io.reactivex.schedulers.Schedulers;

import static com.example.pokeapp.app.MyApp.sApiService;


public class DiscoverFragment extends Fragment {

    private Unbinder mUnbinder;
    @BindView(R.id.rv_pokemon)RecyclerView mRecyclerView;
    @BindView(R.id.progressBar)ProgressBar mProgressBar;

    private CompositeDisposable mCompositeDisposable = new CompositeDisposable();
    private List<Pokemon> mPokemonList = new ArrayList<>();

    private GridLayoutManager mGridLayoutManager;
    private Context mContext;
    private PokemonAdapter mAdapter;

    private boolean mNextLoading = false;
    private int mOffset = 0;
    private int mTotalItemCount, mLastVisibleItem;


    public DiscoverFragment() {
    }

    public static DiscoverFragment newInstance() {
        return new DiscoverFragment();
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        mContext = context;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_pokemon, container, false);
        mUnbinder = ButterKnife.bind(this, rootView);

        mAdapter = PokemonAdapter.getInstance(mContext, "discover");
        mGridLayoutManager = new GridLayoutManager(mContext, 2);
        mRecyclerView.setLayoutManager(mGridLayoutManager);
        mRecyclerView.setHasFixedSize(true);
        mRecyclerView.setAdapter(mAdapter);

        setUpLoadMoreListener();
        fetchAllPokemonsWithPagination(mOffset);
        return rootView;
    }


    private void setUpLoadMoreListener(){
        mRecyclerView.addOnScrollListener(new OnScrollListener() {
            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);

                if(dy > 0) {
                    mTotalItemCount = mGridLayoutManager.getItemCount();
                    mLastVisibleItem = mGridLayoutManager.findFirstVisibleItemPosition();
                    mLastVisibleItem = mGridLayoutManager.findLastVisibleItemPosition();

                    if (!mNextLoading && (mTotalItemCount - mLastVisibleItem) <=10) {
                        mOffset+= 20;
                        fetchAllPokemonsWithPagination(mOffset);
                        mNextLoading = true;
                    }
                }
            }
        });
    }

    private void fetchAllPokemonsWithPagination(int offset){
        List<Pokemon> trans = new ArrayList<>();
        mProgressBar.setVisibility(View.VISIBLE);
        mCompositeDisposable.add(
                sApiService.fetchPokemonsWithPagination(20, offset)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeWith(new DisposableSingleObserver<PokemonList>(){
                    @Override
                    public void onSuccess(PokemonList pokemons){
                        mNextLoading = false;
                        mPokemonList.clear();
                        mPokemonList.addAll(pokemons.getResults());
                        for (Pokemon pokemon : mPokemonList){
                            pokemon.setOrder(pokemon.getNumber());
                            trans.add(pokemon);
                        }
                        mAdapter.setList(trans);
                        mProgressBar.setVisibility(View.INVISIBLE);
                    }
                    @Override
                    public void onError(Throwable e) {
                        Log.e("tag", "onError: " + e.getMessage());
                    }
                })
        );
    }


    @Override public void onDestroyView() {
        super.onDestroyView();
        mUnbinder.unbind();
        mCompositeDisposable.clear();
    }
}