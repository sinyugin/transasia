package com.example.pokeapp;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.example.pokeapp.adapter.PokemonAdapter;
import com.example.pokeapp.db.DatabaseManager;
import com.example.pokeapp.model.Pokemon;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;


public class PokedexFragment extends Fragment {

    private Unbinder mUnbinder;
    @BindView(R.id.rv_pokemon)RecyclerView mRecyclerView;
    @BindView(R.id.progressBar)ProgressBar mProgressBar;

    private Context mContext;
    private PokemonAdapter mAdapter;
    private DatabaseManager mManager;

    public PokedexFragment() {
    }

    public static PokedexFragment newInstance() {
        return new PokedexFragment();
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        mContext = context;
        mManager = DatabaseManager.getInstance(mContext);
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_pokemon, container, false);
        mUnbinder = ButterKnife.bind(this, rootView);

        mAdapter = PokemonAdapter.getInstance(mContext, "pokedex");
        GridLayoutManager gridLayoutManager = new GridLayoutManager(mContext, 2);
        mRecyclerView.setLayoutManager(gridLayoutManager);
        mRecyclerView.setHasFixedSize(true);
        mRecyclerView.setAdapter(mAdapter);
        return rootView;
    }

    @Override
    public void onResume() {
        super.onResume();
        getPokemons();
    }

    @Override public void onDestroyView() {
        super.onDestroyView();
        mUnbinder.unbind();
    }

    private void getPokemons() {
        List<Pokemon> pokemonList = mManager.getPokemons();
        mAdapter.setList(pokemonList);
        mProgressBar.setVisibility(View.INVISIBLE);
    }
}