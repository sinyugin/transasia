package com.example.pokeapp.app;



public class MyConstants {

    //Retrofit
    public static final String BASE_URL = "http://pokeapi.co/api/v2/";
    public static final int REQUEST_TIMEOUT = 60;

    //DB name
    public static final String DB_NAME = "app.db";

    //Table name
    public static final String POKEMON_TABLE = "tbl_pokemons";
    public static final int TABLE_VERSION = 1;

    //Table fields name and Detail Activity bundle arguments
    public static final String NUMBER = "pkm_number";
    public static final String NAME = "pkm_name";
    public static final String SPRITE = "pkm_sprite";
    public static final String SPEED = "pkm_speed";
    public static final String SPECIAL_DEFENCE = "pkm_special_defense";
    public static final String SPECIAL_ATTACK = "pkm_special_attack";
    public static final String ABILITY1 = "pkm_ability1";
    public static final String ABILITY2 = "pkm_ability2";
    public static final String ABILITY3 = "pkm_ability3";

    //Bundle arguments
    public static final String ARG_DETAIL_ACTIVITY_NUMBER = "pokemon_number";
    public static final String ARG_DETAIL_ACTIVITY_STATE = "pokemon_state";


    public static String createPokemonTable(){
        String command = "CREATE TABLE " + POKEMON_TABLE +
                " (" +
                " _id integer primary key autoincrement, " +
                NUMBER + " integer, " +
                NAME + ", " +
                SPRITE + ", " +
                SPEED + ", " +
                SPECIAL_DEFENCE + ", " +
                SPECIAL_ATTACK + ", " +
                ABILITY1 + ", " +
                ABILITY2 + ", " +
                ABILITY3 +
                " )";

        return command;
    }
}
