package com.example.pokeapp.app;


import com.example.pokeapp.DetailActivity;
import com.example.pokeapp.DetailStreamActivity;

import dagger.Module;
import dagger.android.ContributesAndroidInjector;

@Module
public abstract class MyAppModule {
    @ContributesAndroidInjector
//    abstract DetailActivity contributeActivityInjector();
    abstract DetailStreamActivity contributeActivityInjector();
}
