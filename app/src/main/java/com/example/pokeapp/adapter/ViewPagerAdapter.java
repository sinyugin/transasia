package com.example.pokeapp.adapter;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

import com.example.pokeapp.DiscoverFragment;
import com.example.pokeapp.PokedexFragment;


public class ViewPagerAdapter extends FragmentPagerAdapter {

    private ViewPagerAdapter(FragmentManager fm) {
        super(fm);
    }

    public static ViewPagerAdapter getInstance(FragmentManager fm){
        return new ViewPagerAdapter(fm);
    }

    @Override
    public int getCount() {
        return 2;
    }
    @Override
    public Fragment getItem(int position) {
        if (position == 0){
            return DiscoverFragment.newInstance();
        } else {
            return PokedexFragment.newInstance();
        }
    }
}
