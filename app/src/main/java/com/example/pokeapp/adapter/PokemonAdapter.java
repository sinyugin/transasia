package com.example.pokeapp.adapter;

import android.content.Context;
import android.net.Uri;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.example.pokeapp.DetailActivity;
import com.example.pokeapp.DetailStreamActivity;
import com.example.pokeapp.R;
import com.example.pokeapp.model.Pokemon;
import com.facebook.drawee.view.SimpleDraweeView;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;


public class PokemonAdapter extends RecyclerView.Adapter<PokemonAdapter.PokemonHolder> {

    private Context mContext;
    private List<Pokemon> mPokemonList = new ArrayList<>();
    private String mType;


    private PokemonAdapter(Context context, String type) {
        mContext = context;
        mType = type;
    }

    public static PokemonAdapter getInstance(Context context, String type){
        return new PokemonAdapter(context, type);
    }

    public void setList(List<Pokemon> list){
        if (mType.equals("discover")){
            mPokemonList.addAll(list);
        } else if (mType.equals("pokedex")){
            mPokemonList = list;
        }
        this.notifyDataSetChanged();
    }

    @NonNull
    @Override
    public PokemonHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.cv_pokemon, parent, false);
        return new PokemonHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull PokemonHolder holder, int position) {
        Pokemon pokemon = mPokemonList.get(position);
        String uriString = prepareUriToSet(pokemon);

        holder.pokemon_sprite.setImageURI(Uri.parse(uriString));
        holder.pokemon_name.setText(pokemon.getName().toUpperCase());
        //holder.pokemon_view.setOnClickListener(v -> DetailActivity.showActivity(mContext, pokemon.getOrder()));
        holder.pokemon_view.setOnClickListener(v -> DetailStreamActivity.showActivity(mContext, pokemon.getOrder()));
    }

    @NonNull
    private String prepareUriToSet(Pokemon pokemon) {
        StringBuilder builder = new StringBuilder();
        builder.append("https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/");
        builder.append(pokemon.getOrder());
        builder.append(".png");
        return builder.toString();
    }

    @Override
    public int getItemCount() {
        return mPokemonList == null ? 0 : mPokemonList.size();
    }

    public class PokemonHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.pokemon_view) RelativeLayout pokemon_view;
        @BindView(R.id.pokemon_sprite) SimpleDraweeView pokemon_sprite;
        @BindView(R.id.pokemon_name) TextView pokemon_name;

        public PokemonHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }
}
